<html>
<head>
    <title>Таблица</title>
    <meta charset="UTF-8">
</head>
<body>
<?php
require 'mytable.php';
require 'dataBase.php';
try {
    // соединение с БД
    $database = new database('localhost', 'test1', 'root', '');
    $newconection = $database->db;

    //вызов класса
    $mytable1 = new mytable($newconection);

    //Добавленние нового пользователя
    $new_user = array('firstname' => 'Сергей', 'secondname' => 'Луконин', 'age' => '29');
    //$mytable1->insert($new_user);

    // вывод данных
    $table = $mytable1->selectAll();
    echo '<pre>', var_dump($table), '</pre>';

    // поиск по id
    $tablesearch = $mytable1->selectById(4);
    echo '<pre>', var_dump($tablesearch), '</pre>';
}

catch(Exception $e) {
    echo 'ERROR: ' . $e->getMessage();
}

?>
</body>
</html>