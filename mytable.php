<?php
class mytable{
    private $conection = null;
    private $table = "mytable";

    public function __construct($conection) {
        $this->conection=$conection;
    }

    public function insert($data) {
        $query = $this->conection->prepare("INSERT INTO " . $this->table . " SET
                                        firstname = '".$data['firstname']."',
                                        secondname = '".$data['secondname']."',
                                        age = '".$data['age']."'");
        $query->execute();

    }

    public function selectAll(){

        $query = $this->conection->prepare("SELECT id, firstname, secondname, age FROM " . $this->table);
        $query->execute();
        return $query->fetchAll();
        }

    public function selectById($id){
        $id = (int)$id;
        $result = false;
        if($id) {
            $query = $this->conection->prepare("SELECT id, firstname, secondname, age FROM " . $this->table . " WHERE id = :id");
            $query->execute(array('id' => $id));
            $result = $query->fetch();
        }
        return $result;
    }

}

?>