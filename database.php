<?php
class dataBase{
    private $db = null;
    private $host   = 'localhost';
    private $dbname = 'test1';
    private $user   = 'root';
    private $pass   = '';


    public function __construct($host, $dbname, $user, $pass) {
        $this->db = new PDO('mysql:host='. $this->host .';dbname='. $this->dbname, $this->user, $this->pass);
        $this->db->exec("set names utf8");
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $this->db;
    }

    function __get($key){
        return $this->$key;
    }

    function __set($key,$value){
        $this->$key=$value;
    }
}